<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Dealer extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_dealer');
	}

	/**
	 * @api {get} /dealer/all Get all dealers.
	 * @apiVersion 0.1.0
	 * @apiName AllDealer 
	 * @apiGroup dealer
	 * @apiHeader {String} X-Api-Key Dealers unique access-key.
	 * @apiPermission Dealer Cant be Accessed permission name : api_dealer_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Dealers.
	 * @apiParam {String} [Field="All Field"] Optional field of Dealers : id, name_thai, name_eng, address_thai, address_eng, img_dealer, tel, created_by, created_at, updated_by, updated_at, status.
	 * @apiParam {String} [Start=0] Optional start index of Dealers.
	 * @apiParam {String} [Limit=10] Optional limit data of Dealers.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of dealer.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataDealer Dealer data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_dealer_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'name_thai', 'name_eng', 'address_thai', 'address_eng', 'img_dealer', 'tel', 'created_by', 'created_at', 'updated_by', 'updated_at', 'status'];
		$dealers = $this->model_api_dealer->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_dealer->count_all($filter, $field);

		$data['dealer'] = $dealers;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Dealer',
			'data'	 	=> $data,
			'total' 	=> $total
		], API::HTTP_OK);
	}

	
	/**
	 * @api {get} /dealer/detail Detail Dealer.
	 * @apiVersion 0.1.0
	 * @apiName DetailDealer
	 * @apiGroup dealer
	 * @apiHeader {String} X-Api-Key Dealers unique access-key.
	 * @apiPermission Dealer Cant be Accessed permission name : api_dealer_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Dealers.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of dealer.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError DealerNotFound Dealer data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_dealer_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'name_thai', 'name_eng', 'address_thai', 'address_eng', 'img_dealer', 'tel', 'created_by', 'created_at', 'updated_by', 'updated_at', 'status'];
		$data['dealer'] = $this->model_api_dealer->find($id, $select_field);

		if ($data['dealer']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Dealer',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Dealer not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /dealer/add Add Dealer.
	 * @apiVersion 0.1.0
	 * @apiName AddDealer
	 * @apiGroup dealer
	 * @apiHeader {String} X-Api-Key Dealers unique access-key.
	 * @apiPermission Dealer Cant be Accessed permission name : api_dealer_add
	 *
 	 * @apiParam {String} Name_thai Mandatory name_thai of Dealers. Input Name Thai Max Length : 50. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Dealers. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Address_thai Mandatory address_thai of Dealers.  
	 * @apiParam {String} Address_eng Mandatory address_eng of Dealers.  
	 * @apiParam {String} Img_dealer Mandatory img_dealer of Dealers. Input Img Dealer Max Length : 255. 
	 * @apiParam {String} Tel Mandatory tel of Dealers. Input Tel Max Length : 10. 
	 * @apiParam {String} Created_by Mandatory created_by of Dealers. Input Created By Max Length : 50. 
	 * @apiParam {String} Created_at Mandatory created_at of Dealers.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Dealers. Input Updated By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Dealers.  
	 * @apiParam {String} Status Mandatory status of Dealers. Input Status Max Length : 10. 
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_dealer_add', false);

		$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('address_thai', 'Address Thai', 'trim|required');
		$this->form_validation->set_rules('address_eng', 'Address Eng', 'trim|required');
		$this->form_validation->set_rules('img_dealer', 'Img Dealer', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('tel', 'Tel', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'address_thai' => $this->input->post('address_thai'),
				'address_eng' => $this->input->post('address_eng'),
				'img_dealer' => $this->input->post('img_dealer'),
				'tel' => $this->input->post('tel'),
				'created_by' => $this->input->post('created_by'),
				'created_at' => $this->input->post('created_at'),
				'updated_by' => $this->input->post('updated_by'),
				'updated_at' => $this->input->post('updated_at'),
				'status' => $this->input->post('status'),
			];
			
			$save_dealer = $this->model_api_dealer->store($save_data);

			if ($save_dealer) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /dealer/update Update Dealer.
	 * @apiVersion 0.1.0
	 * @apiName UpdateDealer
	 * @apiGroup dealer
	 * @apiHeader {String} X-Api-Key Dealers unique access-key.
	 * @apiPermission Dealer Cant be Accessed permission name : api_dealer_update
	 *
	 * @apiParam {String} Name_thai Mandatory name_thai of Dealers. Input Name Thai Max Length : 50. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Dealers. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Address_thai Mandatory address_thai of Dealers.  
	 * @apiParam {String} Address_eng Mandatory address_eng of Dealers.  
	 * @apiParam {String} Img_dealer Mandatory img_dealer of Dealers. Input Img Dealer Max Length : 255. 
	 * @apiParam {String} Tel Mandatory tel of Dealers. Input Tel Max Length : 10. 
	 * @apiParam {String} Created_by Mandatory created_by of Dealers. Input Created By Max Length : 50. 
	 * @apiParam {String} Created_at Mandatory created_at of Dealers.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Dealers. Input Updated By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Dealers.  
	 * @apiParam {String} Status Mandatory status of Dealers. Input Status Max Length : 10. 
	 * @apiParam {Integer} id Mandatory id of Dealer.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */

	public function update_status_post(){
		
		$this->is_allowed('api_dealer_update', false);

		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				
				'status' => $this->input->post('status'),
			];
			
			$save_dealer = $this->model_api_dealer->change($this->post('id'), $save_data);

			if ($save_dealer) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}


	public function update_post()
	{
		$this->is_allowed('api_dealer_update', false);

		
		$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('address_thai', 'Address Thai', 'trim|required');
		$this->form_validation->set_rules('address_eng', 'Address Eng', 'trim|required');
		$this->form_validation->set_rules('img_dealer', 'Img Dealer', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('tel', 'Tel', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'address_thai' => $this->input->post('address_thai'),
				'address_eng' => $this->input->post('address_eng'),
				'img_dealer' => $this->input->post('img_dealer'),
				'tel' => $this->input->post('tel'),
				'created_by' => $this->input->post('created_by'),
				'created_at' => $this->input->post('created_at'),
				'updated_by' => $this->input->post('updated_by'),
				'updated_at' => $this->input->post('updated_at'),
				'status' => $this->input->post('status'),
			];
			
			$save_dealer = $this->model_api_dealer->change($this->post('id'), $save_data);

			if ($save_dealer) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /dealer/delete Delete Dealer. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteDealer
	 * @apiGroup dealer
	 * @apiHeader {String} X-Api-Key Dealers unique access-key.
	 	 * @apiPermission Dealer Cant be Accessed permission name : api_dealer_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Dealers .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_dealer_delete', false);

		$dealer = $this->model_api_dealer->find($this->post('id'));

		if (!$dealer) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Dealer not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_dealer->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Dealer deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Dealer not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

}

/* End of file Dealer.php */
/* Location: ./application/controllers/api/Dealer.php */