<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Service extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_service');
	}

	/**
	 * @api {get} /service/all Get all services.
	 * @apiVersion 0.1.0
	 * @apiName AllService 
	 * @apiGroup service
	 * @apiHeader {String} X-Api-Key Services unique access-key.
	 * @apiPermission Service Cant be Accessed permission name : api_service_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Services.
	 * @apiParam {String} [Field="All Field"] Optional field of Services : id, name_thai, name_eng, detail, img_service, video, created_by, created_at, updated_by, updated_at, status.
	 * @apiParam {String} [Start=0] Optional start index of Services.
	 * @apiParam {String} [Limit=10] Optional limit data of Services.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of service.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataService Service data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_service_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'name_thai', 'name_eng', 'detail', 'detail_eng','img_service', 'video','link_youtube', 'created_by', 'created_at', 'updated_by', 'updated_at', 'status'];
		$services = $this->model_api_service->get($filter, $field,null, $start, $select_field);
		$total = $this->model_api_service->count_all($filter, $field);

		$data['service'] = $services;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Service',
			'data'	 	=> $data,
			'total' 	=> $total
		], API::HTTP_OK);
	}

	public function update_status_post()
	{
		$this->is_allowed('api_service_update', false);

		
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				
				'status' => $this->input->post('status'),
			];
			
			$save_service = $this->model_api_service->change($this->post('id'), $save_data);

			if ($save_service) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {get} /service/detail Detail Service.
	 * @apiVersion 0.1.0
	 * @apiName DetailService
	 * @apiGroup service
	 * @apiHeader {String} X-Api-Key Services unique access-key.
	 * @apiPermission Service Cant be Accessed permission name : api_service_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Services.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of service.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ServiceNotFound Service data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_service_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'name_thai', 'name_eng', 'detail', 'detail_eng','img_service', 'video','link_youtube','created_by', 'created_at', 'updated_by', 'updated_at', 'status'];
		$data['service'] = $this->model_api_service->find($id, $select_field);

		if ($data['service']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Service',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Service not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /service/add Add Service.
	 * @apiVersion 0.1.0
	 * @apiName AddService
	 * @apiGroup service
	 * @apiHeader {String} X-Api-Key Services unique access-key.
	 * @apiPermission Service Cant be Accessed permission name : api_service_add
	 *
 	 * @apiParam {String} Name_thai Mandatory name_thai of Services. Input Name Thai Max Length : 30. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Services. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Detail Mandatory detail of Services.  
	 * @apiParam {String} Img_service Mandatory img_service of Services. Input Img Service Max Length : 255. 
	 * @apiParam {String} Video Mandatory video of Services. Input Video Max Length : 255. 
	 * @apiParam {String} Created_by Mandatory created_by of Services. Input Created By Max Length : 50. 
	 * @apiParam {String} Created_at Mandatory created_at of Services.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Services. Input Updated By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Services.  
	 * @apiParam {String} Status Mandatory status of Services. Input Status Max Length : 10. 
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_service_add', false);

		$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[30]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
		$this->form_validation->set_rules('img_service', 'Img Service', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('video', 'Video', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'detail' => $this->input->post('detail'),
				'img_service' => $this->input->post('img_service'),
				'video' => $this->input->post('video'),
				'created_by' => $this->input->post('created_by'),
				'created_at' => $this->input->post('created_at'),
				'updated_by' => $this->input->post('updated_by'),
				'updated_at' => $this->input->post('updated_at'),
				'status' => $this->input->post('status'),
			];
			
			$save_service = $this->model_api_service->store($save_data);

			if ($save_service) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /service/update Update Service.
	 * @apiVersion 0.1.0
	 * @apiName UpdateService
	 * @apiGroup service
	 * @apiHeader {String} X-Api-Key Services unique access-key.
	 * @apiPermission Service Cant be Accessed permission name : api_service_update
	 *
	 * @apiParam {String} Name_thai Mandatory name_thai of Services. Input Name Thai Max Length : 30. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Services. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Detail Mandatory detail of Services.  
	 * @apiParam {String} Img_service Mandatory img_service of Services. Input Img Service Max Length : 255. 
	 * @apiParam {String} Video Mandatory video of Services. Input Video Max Length : 255. 
	 * @apiParam {String} Created_by Mandatory created_by of Services. Input Created By Max Length : 50. 
	 * @apiParam {String} Created_at Mandatory created_at of Services.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Services. Input Updated By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Services.  
	 * @apiParam {String} Status Mandatory status of Services. Input Status Max Length : 10. 
	 * @apiParam {Integer} id Mandatory id of Service.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_post()
	{
		$this->is_allowed('api_service_update', false);

		
		$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[30]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
		$this->form_validation->set_rules('img_service', 'Img Service', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('video', 'Video', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'detail' => $this->input->post('detail'),
				'img_service' => $this->input->post('img_service'),
				'video' => $this->input->post('video'),
				'created_by' => $this->input->post('created_by'),
				'created_at' => $this->input->post('created_at'),
				'updated_by' => $this->input->post('updated_by'),
				'updated_at' => $this->input->post('updated_at'),
				'status' => $this->input->post('status'),
			];
			
			$save_service = $this->model_api_service->change($this->post('id'), $save_data);

			if ($save_service) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /service/delete Delete Service. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteService
	 * @apiGroup service
	 * @apiHeader {String} X-Api-Key Services unique access-key.
	 	 * @apiPermission Service Cant be Accessed permission name : api_service_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Services .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_service_delete', false);

		$service = $this->model_api_service->find($this->post('id'));

		if (!$service) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Service not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_service->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Service deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Service not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

}

/* End of file Service.php */
/* Location: ./application/controllers/api/Service.php */