<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Color extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_color');
	}

	/**
	 * @api {get} /color/all Get all colors.
	 * @apiVersion 0.1.0
	 * @apiName AllColor 
	 * @apiGroup color
	 * @apiHeader {String} X-Api-Key Colors unique access-key.
	 * @apiPermission Color Cant be Accessed permission name : api_color_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Colors.
	 * @apiParam {String} [Field="All Field"] Optional field of Colors : id, name_thai, name_eng, img_color, created_at, created_by, updated_at, updated_by, status.
	 * @apiParam {String} [Start=0] Optional start index of Colors.
	 * @apiParam {String} [Limit=10] Optional limit data of Colors.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of color.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataColor Color data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function NameById_post(){

		$data = $this->model_api_color->NameById_data($_POST['color_id']);

		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Color',
			'data'	 	=> $data,
		], API::HTTP_OK);


	}
	public function all_get()
	{
		$this->is_allowed('api_color_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'name_thai', 'name_eng', 'img_color', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status'];
		$colors = $this->model_api_color->get($filter, $field, null, $start, $select_field);
		$total = $this->model_api_color->count_all($filter, $field);

		$data['color'] = $colors;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Color',
			'data'	 	=> $data,
			'total' 	=> $total
		], API::HTTP_OK);
	}

	
	/**
	 * @api {get} /color/detail Detail Color.
	 * @apiVersion 0.1.0
	 * @apiName DetailColor
	 * @apiGroup color
	 * @apiHeader {String} X-Api-Key Colors unique access-key.
	 * @apiPermission Color Cant be Accessed permission name : api_color_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Colors.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of color.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ColorNotFound Color data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_color_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'name_thai', 'name_eng', 'img_color', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status'];
		$data['color'] = $this->model_api_color->find($id, $select_field);

		if ($data['color']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Color',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Color not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /color/add Add Color.
	 * @apiVersion 0.1.0
	 * @apiName AddColor
	 * @apiGroup color
	 * @apiHeader {String} X-Api-Key Colors unique access-key.
	 * @apiPermission Color Cant be Accessed permission name : api_color_add
	 *
 	 * @apiParam {String} Name_thai Mandatory name_thai of Colors. Input Name Thai Max Length : 50. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Colors. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Img_color Mandatory img_color of Colors. Input Img Color Max Length : 255. 
	 * @apiParam {String} Created_at Mandatory created_at of Colors.  
	 * @apiParam {String} Created_by Mandatory created_by of Colors. Input Created By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Colors.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Colors. Input Updated By Max Length : 50. 
	 * @apiParam {String} Status Mandatory status of Colors. Input Status Max Length : 10. 
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_color_add', false);

		$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('img_color', 'Img Color', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'img_color' => $this->input->post('img_color'),
				'created_at' => $this->input->post('created_at'),
				'created_by' => $this->input->post('created_by'),
				'updated_at' => $this->input->post('updated_at'),
				'updated_by' => $this->input->post('updated_by'),
				'status' => $this->input->post('status'),
			];
			
			$save_color = $this->model_api_color->store($save_data);

			if ($save_color) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /color/update Update Color.
	 * @apiVersion 0.1.0
	 * @apiName UpdateColor
	 * @apiGroup color
	 * @apiHeader {String} X-Api-Key Colors unique access-key.
	 * @apiPermission Color Cant be Accessed permission name : api_color_update
	 *
	 * @apiParam {String} Name_thai Mandatory name_thai of Colors. Input Name Thai Max Length : 50. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Colors. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Img_color Mandatory img_color of Colors. Input Img Color Max Length : 255. 
	 * @apiParam {String} Created_at Mandatory created_at of Colors.  
	 * @apiParam {String} Created_by Mandatory created_by of Colors. Input Created By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Colors.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Colors. Input Updated By Max Length : 50. 
	 * @apiParam {String} Status Mandatory status of Colors. Input Status Max Length : 10. 
	 * @apiParam {Integer} id Mandatory id of Color.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_post()
	{
		$this->is_allowed('api_color_update', false);

		
		$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('img_color', 'Img Color', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'img_color' => $this->input->post('img_color'),
				'created_at' => $this->input->post('created_at'),
				'created_by' => $this->input->post('created_by'),
				'updated_at' => $this->input->post('updated_at'),
				'updated_by' => $this->input->post('updated_by'),
				'status' => $this->input->post('status'),
			];
			
			$save_color = $this->model_api_color->change($this->post('id'), $save_data);

			if ($save_color) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function update_status_post()
	{
		$this->is_allowed('api_color_update', false);

		
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				
				'status' => $this->input->post('status'),
			];
			
			$save_color = $this->model_api_color->change($this->post('id'), $save_data);

			if ($save_color) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /color/delete Delete Color. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteColor
	 * @apiGroup color
	 * @apiHeader {String} X-Api-Key Colors unique access-key.
	 	 * @apiPermission Color Cant be Accessed permission name : api_color_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Colors .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_color_delete', false);

		$color = $this->model_api_color->find($this->post('id'));

		if (!$color) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Color not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_color->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Color deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Color not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

}

/* End of file Color.php */
/* Location: ./application/controllers/api/Color.php */