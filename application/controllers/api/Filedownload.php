<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Filedownload extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_filedownload');
	}

	/**
	 * @api {get} /filedownload/all Get all filedownloads.
	 * @apiVersion 0.1.0
	 * @apiName AllFiledownload 
	 * @apiGroup filedownload
	 * @apiHeader {String} X-Api-Key Filedownloads unique access-key.
	 * @apiPermission Filedownload Cant be Accessed permission name : api_filedownload_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Filedownloads.
	 * @apiParam {String} [Field="All Field"] Optional field of Filedownloads : id, name_thai, name_eng, file, created_by, created_at, updated_by, updated_at, status.
	 * @apiParam {String} [Start=0] Optional start index of Filedownloads.
	 * @apiParam {String} [Limit=10] Optional limit data of Filedownloads.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of filedownload.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataFiledownload Filedownload data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_action_off_detail_post(){

		$data_array = array(
            'status' => 'off',
        );
        $this->db->where('type = "'.$_POST['type'].'" ')->where('id != "'.$_POST['id'].'"');
       
		$result = $this->db->update('filedownload',$data_array);
		
		if($result == true){
            
			$this->response([
				'status' 	=> true,
				'data' => 'updated data done',
			], API::HTTP_OK);

		}else{
			$this->response([
				'status' 	=> false,
				'data'	 	=> 'updated data failed',
			], API::HTTP_OK);
		}

	}


	public function update_status_post()
	{
		//$this->is_allowed('api_filedownload_update', false);

		
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				
				'status' => $this->input->post('status'),
			];
			
			$save_filedownload = $this->model_api_filedownload->change($this->post('id'), $save_data);

			if ($save_filedownload) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function all_get()
	{
		$this->is_allowed('api_filedownload_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'name_thai', 'name_eng', 'file','type', 'created_by', 'created_at', 'updated_by', 'updated_at', 'status'];
		$filedownloads = $this->model_api_filedownload->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_filedownload->count_all($filter, $field);

		$data['filedownload'] = $filedownloads;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Filedownload',
			'data'	 	=> $data,
			'total' 	=> $total
		], API::HTTP_OK);
	}

	
	/**
	 * @api {get} /filedownload/detail Detail Filedownload.
	 * @apiVersion 0.1.0
	 * @apiName DetailFiledownload
	 * @apiGroup filedownload
	 * @apiHeader {String} X-Api-Key Filedownloads unique access-key.
	 * @apiPermission Filedownload Cant be Accessed permission name : api_filedownload_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Filedownloads.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of filedownload.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError FiledownloadNotFound Filedownload data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_filedownload_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'name_thai', 'name_eng', 'file','type',  'created_by', 'created_at', 'updated_by', 'updated_at', 'status'];
		$data['filedownload'] = $this->model_api_filedownload->find($id, $select_field);

		if ($data['filedownload']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Filedownload',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Filedownload not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /filedownload/add Add Filedownload.
	 * @apiVersion 0.1.0
	 * @apiName AddFiledownload
	 * @apiGroup filedownload
	 * @apiHeader {String} X-Api-Key Filedownloads unique access-key.
	 * @apiPermission Filedownload Cant be Accessed permission name : api_filedownload_add
	 *
 	 * @apiParam {String} Name_thai Mandatory name_thai of Filedownloads. Input Name Thai Max Length : 100. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Filedownloads. Input Name Eng Max Length : 100. 
	 * @apiParam {String} File Mandatory file of Filedownloads. Input File Max Length : 255. 
	 * @apiParam {String} Created_by Mandatory created_by of Filedownloads. Input Created By Max Length : 50. 
	 * @apiParam {String} Created_at Mandatory created_at of Filedownloads.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Filedownloads. Input Updated By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Filedownloads.  
	 * @apiParam {String} Status Mandatory status of Filedownloads. Input Status Max Length : 10. 
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_filedownload_add', false);

		$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('file', 'File', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'file' => $this->input->post('file'),
				'created_by' => $this->input->post('created_by'),
				'created_at' => $this->input->post('created_at'),
				'updated_by' => $this->input->post('updated_by'),
				'updated_at' => $this->input->post('updated_at'),
				'status' => $this->input->post('status'),
			];
			
			$save_filedownload = $this->model_api_filedownload->store($save_data);

			if ($save_filedownload) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /filedownload/update Update Filedownload.
	 * @apiVersion 0.1.0
	 * @apiName UpdateFiledownload
	 * @apiGroup filedownload
	 * @apiHeader {String} X-Api-Key Filedownloads unique access-key.
	 * @apiPermission Filedownload Cant be Accessed permission name : api_filedownload_update
	 *
	 * @apiParam {String} Name_thai Mandatory name_thai of Filedownloads. Input Name Thai Max Length : 100. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Filedownloads. Input Name Eng Max Length : 100. 
	 * @apiParam {String} File Mandatory file of Filedownloads. Input File Max Length : 255. 
	 * @apiParam {String} Created_by Mandatory created_by of Filedownloads. Input Created By Max Length : 50. 
	 * @apiParam {String} Created_at Mandatory created_at of Filedownloads.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Filedownloads. Input Updated By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Filedownloads.  
	 * @apiParam {String} Status Mandatory status of Filedownloads. Input Status Max Length : 10. 
	 * @apiParam {Integer} id Mandatory id of Filedownload.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_post()
	{
		$this->is_allowed('api_filedownload_update', false);

		
		$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('file', 'File', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'file' => $this->input->post('file'),
				'created_by' => $this->input->post('created_by'),
				'created_at' => $this->input->post('created_at'),
				'updated_by' => $this->input->post('updated_by'),
				'updated_at' => $this->input->post('updated_at'),
				'status' => $this->input->post('status'),
			];
			
			$save_filedownload = $this->model_api_filedownload->change($this->post('id'), $save_data);

			if ($save_filedownload) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /filedownload/delete Delete Filedownload. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteFiledownload
	 * @apiGroup filedownload
	 * @apiHeader {String} X-Api-Key Filedownloads unique access-key.
	 	 * @apiPermission Filedownload Cant be Accessed permission name : api_filedownload_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Filedownloads .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_filedownload_delete', false);

		$filedownload = $this->model_api_filedownload->find($this->post('id'));

		if (!$filedownload) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Filedownload not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_filedownload->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Filedownload deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Filedownload not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

}

/* End of file Filedownload.php */
/* Location: ./application/controllers/api/Filedownload.php */