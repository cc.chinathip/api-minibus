<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Activity extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_activity');
	}

	/**
	 * @api {get} /activity/all Get all activitys.
	 * @apiVersion 0.1.0
	 * @apiName AllActivity 
	 * @apiGroup activity
	 * @apiHeader {String} X-Api-Key Activitys unique access-key.
	 * @apiPermission Activity Cant be Accessed permission name : api_activity_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Activitys.
	 * @apiParam {String} [Field="All Field"] Optional field of Activitys : id, name_thai, name_eng, detail, activity_start, activity_end, img_activity, created_at, created_by, updated_at, updated_by, status.
	 * @apiParam {String} [Start=0] Optional start index of Activitys.
	 * @apiParam {String} [Limit=10] Optional limit data of Activitys.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of activity.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataActivity Activity data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		//$this->is_allowed('api_activity_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : null;
		$start = $this->get('start');

		$select_field = ['id', 'name_thai', 'name_eng', 'detail', 'detail_eng','activity_start', 'activity_end', 'img_activity', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status'];
		$activitys = $this->model_api_activity->get($filter, $field,$limit, $start, $select_field);
		$total = $this->model_api_activity->count_all($filter, $field);

		$data['activity'] = $activitys;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Activity',
			'data'	 	=> $data,
			'total' 	=> $total
		], API::HTTP_OK);
	}

	
	/**
	 * @api {get} /activity/detail Detail Activity.
	 * @apiVersion 0.1.0
	 * @apiName DetailActivity
	 * @apiGroup activity
	 * @apiHeader {String} X-Api-Key Activitys unique access-key.
	 * @apiPermission Activity Cant be Accessed permission name : api_activity_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Activitys.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of activity.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ActivityNotFound Activity data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_activity_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'name_thai', 'name_eng', 'detail', 'detail_eng','activity_start', 'activity_end', 'img_activity', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status'];
		$data['activity'] = $this->model_api_activity->find($id, $select_field);

		if ($data['activity']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Activity',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Activity not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /activity/add Add Activity.
	 * @apiVersion 0.1.0
	 * @apiName AddActivity
	 * @apiGroup activity
	 * @apiHeader {String} X-Api-Key Activitys unique access-key.
	 * @apiPermission Activity Cant be Accessed permission name : api_activity_add
	 *
 	 * @apiParam {String} Name_thai Mandatory name_thai of Activitys. Input Name Thai Max Length : 50. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Activitys. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Detail Mandatory detail of Activitys.  
	 * @apiParam {String} Activity_start Mandatory activity_start of Activitys.  
	 * @apiParam {String} Activity_end Mandatory activity_end of Activitys.  
	 * @apiParam {String} Img_activity Mandatory img_activity of Activitys. Input Img Activity Max Length : 255. 
	 * @apiParam {String} Created_at Mandatory created_at of Activitys.  
	 * @apiParam {String} Created_by Mandatory created_by of Activitys. Input Created By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Activitys.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Activitys. Input Updated By Max Length : 50. 
	 * @apiParam {String} Status Mandatory status of Activitys. Input Status Max Length : 10. 
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_activity_add', false);

		$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
		$this->form_validation->set_rules('activity_start', 'Activity Start', 'trim|required');
		$this->form_validation->set_rules('activity_end', 'Activity End', 'trim|required');
		$this->form_validation->set_rules('img_activity', 'Img Activity', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'detail' => $this->input->post('detail'),
				'activity_start' => $this->input->post('activity_start'),
				'activity_end' => $this->input->post('activity_end'),
				'img_activity' => $this->input->post('img_activity'),
				'created_at' => $this->input->post('created_at'),
				'created_by' => $this->input->post('created_by'),
				'updated_at' => $this->input->post('updated_at'),
				'updated_by' => $this->input->post('updated_by'),
				'status' => $this->input->post('status'),
			];
			
			$save_activity = $this->model_api_activity->store($save_data);

			if ($save_activity) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /activity/update Update Activity.
	 * @apiVersion 0.1.0
	 * @apiName UpdateActivity
	 * @apiGroup activity
	 * @apiHeader {String} X-Api-Key Activitys unique access-key.
	 * @apiPermission Activity Cant be Accessed permission name : api_activity_update
	 *
	 * @apiParam {String} Name_thai Mandatory name_thai of Activitys. Input Name Thai Max Length : 50. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Activitys. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Detail Mandatory detail of Activitys.  
	 * @apiParam {String} Activity_start Mandatory activity_start of Activitys.  
	 * @apiParam {String} Activity_end Mandatory activity_end of Activitys.  
	 * @apiParam {String} Img_activity Mandatory img_activity of Activitys. Input Img Activity Max Length : 255. 
	 * @apiParam {String} Created_at Mandatory created_at of Activitys.  
	 * @apiParam {String} Created_by Mandatory created_by of Activitys. Input Created By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Activitys.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Activitys. Input Updated By Max Length : 50. 
	 * @apiParam {String} Status Mandatory status of Activitys. Input Status Max Length : 10. 
	 * @apiParam {Integer} id Mandatory id of Activity.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_status_post()
	{
		$this->is_allowed('api_activity_update', false);

		
		/* $this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
		$this->form_validation->set_rules('activity_start', 'Activity Start', 'trim|required');
		$this->form_validation->set_rules('activity_end', 'Activity End', 'trim|required');
		$this->form_validation->set_rules('img_activity', 'Img Activity', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]'); */
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				/* 'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'detail' => $this->input->post('detail'),
				'activity_start' => $this->input->post('activity_start'),
				'activity_end' => $this->input->post('activity_end'),
				'img_activity' => $this->input->post('img_activity'),
				'created_at' => $this->input->post('created_at'),
				'created_by' => $this->input->post('created_by'),
				'updated_at' => $this->input->post('updated_at'),
				'updated_by' => $this->input->post('updated_by'), */
				'status' => $this->input->post('status'),
			];
			
			$save_activity = $this->model_api_activity->change($this->post('id'), $save_data);

			if ($save_activity) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /activity/delete Delete Activity. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteActivity
	 * @apiGroup activity
	 * @apiHeader {String} X-Api-Key Activitys unique access-key.
	 	 * @apiPermission Activity Cant be Accessed permission name : api_activity_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Activitys .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_activity_delete', false);

		$activity = $this->model_api_activity->find($this->post('id'));

		if (!$activity) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Activity not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_activity->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Activity deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Activity not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

}

/* End of file Activity.php */
/* Location: ./application/controllers/api/Activity.php */