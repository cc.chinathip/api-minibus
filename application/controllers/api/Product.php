<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Product extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_product');
	}

	/**
	 * @api {get} /product/all Get all products.
	 * @apiVersion 0.1.0
	 * @apiName AllProduct 
	 * @apiGroup product
	 * @apiHeader {String} X-Api-Key Products unique access-key.
	 * @apiPermission Product Cant be Accessed permission name : api_product_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Products.
	 * @apiParam {String} [Field="All Field"] Optional field of Products : id, name_thai, name_eng, detail, price, qty, img_product, type_product, created_at, created_by, updated_at, updated_by, status.
	 * @apiParam {String} [Start=0] Optional start index of Products.
	 * @apiParam {String} [Limit=10] Optional limit data of Products.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of product.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataProduct Product data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_product_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'name_thai', 'name_eng', 'detail', 'detail_eng','color_product', 'price', 'qty', 'img_product', 'type_product', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status', 'Exterior', 'Utility', 'Performance', 'Safety'];
		$products = $this->model_api_product->get($filter, $field,null, $start, $select_field);
		$total = $this->model_api_product->count_all($filter, $field);

		$data['product'] = $products;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Product',
			'data'	 	=> $data,
			'total' 	=> $total
		], API::HTTP_OK);
	}


	public function update_action_off_detail_post(){

		$data_array = array(
            'status' => 'off',
        );
        $this->db->where('type_product = "'.$_POST['type'].'" ')->where('id != "'.$_POST['id'].'"');
       
		$result = $this->db->update('product',$data_array);
		
		if($result == true){
            
			$this->response([
				'status' 	=> true,
				'data' => 'updated data done',
			], API::HTTP_OK);

		}else{
			$this->response([
				'status' 	=> false,
				'data'	 	=> 'updated data failed',
			], API::HTTP_OK);
		}

	}
	
	/**
	 * @api {get} /product/detail Detail Product.
	 * @apiVersion 0.1.0
	 * @apiName DetailProduct
	 * @apiGroup product
	 * @apiHeader {String} X-Api-Key Products unique access-key.
	 * @apiPermission Product Cant be Accessed permission name : api_product_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Products.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of product.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ProductNotFound Product data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_product_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'name_thai', 'name_eng', 'detail','detail_eng','color_product','price', 'qty', 'img_product', 'type_product', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status' , 'Exterior', 'Utility', 'Performance', 'Safety'];
		$data['product'] = $this->model_api_product->find($id, $select_field);

		if ($data['product']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Product',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Product not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}


	public function detail_ByType_post()
	{
		$this->is_allowed('api_product_detail', false);
		if(isset($_POST['type_product'])){

			$data = $this->model_api_product->allByType_data($_POST['type_product']);

			if($data){
			
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Detail Product',
					'data'	 	=> $data
				], API::HTTP_OK);
			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> 'Product not found'
				], API::HTTP_NOT_ACCEPTABLE);
			}

		}else{

			$this->response([
				'status' 	=> false,
				'message' 	=> 'not found params'
			], API::HTTP_NOT_ACCEPTABLE);

		}
		
		
		
	}

	
	/**
	 * @api {post} /product/add Add Product.
	 * @apiVersion 0.1.0
	 * @apiName AddProduct
	 * @apiGroup product
	 * @apiHeader {String} X-Api-Key Products unique access-key.
	 * @apiPermission Product Cant be Accessed permission name : api_product_add
	 *
 	 * @apiParam {String} Name_thai Mandatory name_thai of Products. Input Name Thai Max Length : 50. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Products. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Detail Mandatory detail of Products.  
	 * @apiParam {String} Price Mandatory price of Products. Input Price Max Length : 10. 
	 * @apiParam {String} Qty Mandatory qty of Products. Input Qty Max Length : 10. 
	 * @apiParam {String} Img_product Mandatory img_product of Products. Input Img Product Max Length : 255. 
	 * @apiParam {String} Type_product Mandatory type_product of Products. Input Type Product Max Length : 50. 
	 * @apiParam {String} Created_at Mandatory created_at of Products.  
	 * @apiParam {String} Created_by Mandatory created_by of Products. Input Created By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Products.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Products. Input Updated By Max Length : 50. 
	 * @apiParam {String} Status Mandatory status of Products. Input Status Max Length : 10. 
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_product_add', false);

		$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('qty', 'Qty', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('img_product', 'Img Product', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('type_product', 'Type Product', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'detail' => $this->input->post('detail'),
				'price' => $this->input->post('price'),
				'qty' => $this->input->post('qty'),
				'img_product' => $this->input->post('img_product'),
				'type_product' => $this->input->post('type_product'),
				'created_at' => $this->input->post('created_at'),
				'created_by' => $this->input->post('created_by'),
				'updated_at' => $this->input->post('updated_at'),
				'updated_by' => $this->input->post('updated_by'),
				'status' => $this->input->post('status'),
			];
			
			$save_product = $this->model_api_product->store($save_data);

			if ($save_product) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /product/update Update Product.
	 * @apiVersion 0.1.0
	 * @apiName UpdateProduct
	 * @apiGroup product
	 * @apiHeader {String} X-Api-Key Products unique access-key.
	 * @apiPermission Product Cant be Accessed permission name : api_product_update
	 *
	 * @apiParam {String} Name_thai Mandatory name_thai of Products. Input Name Thai Max Length : 50. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Products. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Detail Mandatory detail of Products.  
	 * @apiParam {String} Price Mandatory price of Products. Input Price Max Length : 10. 
	 * @apiParam {String} Qty Mandatory qty of Products. Input Qty Max Length : 10. 
	 * @apiParam {String} Img_product Mandatory img_product of Products. Input Img Product Max Length : 255. 
	 * @apiParam {String} Type_product Mandatory type_product of Products. Input Type Product Max Length : 50. 
	 * @apiParam {String} Created_at Mandatory created_at of Products.  
	 * @apiParam {String} Created_by Mandatory created_by of Products. Input Created By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Products.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Products. Input Updated By Max Length : 50. 
	 * @apiParam {String} Status Mandatory status of Products. Input Status Max Length : 10. 
	 * @apiParam {Integer} id Mandatory id of Product.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */

	/* public function update_data_post()
	{
		
		$save_data = [
			'name_thai' => $this->input->post('name_thai'),
			'name_eng' => $this->input->post('name_eng'),
			'detail' => $this->input->post('detail'),
			'price' => $this->input->post('price'),
			'qty' => $this->input->post('qty'),
			'img_product' => $this->input->post('img_product'),
			'type_product' => $this->input->post('type_product'),
			'updated_at' => date('Y-m-d H:i:s'),
			'updated_by' => $this->input->post('updated_by'),
			'status' => $this->input->post('status'),
		];
		
		$save_product = $this->model_api_product->change($this->post('id'), $save_data);

		if ($save_product) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Your data has been successfully updated into the database'
			], API::HTTP_OK);

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> cclang('data_not_change')
			], API::HTTP_NOT_ACCEPTABLE);
		}
	} */

	public function update_status_post()
	{
		//$this->is_allowed('api_product_update', false);

		
	/* 	$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('qty', 'Qty', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('img_product', 'Img Product', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('type_product', 'Type Product', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]'); */
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				/* 'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'detail' => $this->input->post('detail'),
				'price' => $this->input->post('price'),
				'qty' => $this->input->post('qty'),
				'img_product' => $this->input->post('img_product'),
				'type_product' => $this->input->post('type_product'),
				'created_at' => $this->input->post('created_at'),
				'created_by' => $this->input->post('created_by'),
				'updated_at' => $this->input->post('updated_at'),
				'updated_by' => $this->input->post('updated_by'), */
				'status' => $this->input->post('status'),
			];
			
			$save_product = $this->model_api_product->change($this->post('id'), $save_data);

			if ($save_product) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /product/delete Delete Product. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteProduct
	 * @apiGroup product
	 * @apiHeader {String} X-Api-Key Products unique access-key.
	 	 * @apiPermission Product Cant be Accessed permission name : api_product_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Products .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_product_delete', false);

		$product = $this->model_api_product->find($this->post('id'));

		if (!$product) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Product not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_product->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Product deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Product not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

}

/* End of file Product.php */
/* Location: ./application/controllers/api/Product.php */