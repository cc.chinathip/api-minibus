<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Article extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_article');
	}

	/**
	 * @api {get} /article/all Get all articles.
	 * @apiVersion 0.1.0
	 * @apiName AllArticle 
	 * @apiGroup article
	 * @apiHeader {String} X-Api-Key Articles unique access-key.
	 * @apiPermission Article Cant be Accessed permission name : api_article_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Articles.
	 * @apiParam {String} [Field="All Field"] Optional field of Articles : id, name_thai, name_eng, detail, img_article, video, created_by, created_at, updated_by, updated_at.
	 * @apiParam {String} [Start=0] Optional start index of Articles.
	 * @apiParam {String} [Limit=10] Optional limit data of Articles.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of article.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataArticle Article data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		//$this->is_allowed('api_article_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : null;
		$start = $this->get('start');

		$select_field = ['id', 'name_thai', 'name_eng', 'detail', 'detail_eng','img_article', 'video','link_youtube', 'created_by', 'created_at', 'updated_by', 'updated_at','status'];
		$articles = $this->model_api_article->get($filter, $field,$limit, $start, $select_field);
		$total = $this->model_api_article->count_all($filter, $field);

		$data['article'] = $articles;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Article',
			'data'	 	=> $data,
			'total' 	=> $total
		], API::HTTP_OK);
	}

	
	/**
	 * @api {get} /article/detail Detail Article.
	 * @apiVersion 0.1.0
	 * @apiName DetailArticle
	 * @apiGroup article
	 * @apiHeader {String} X-Api-Key Articles unique access-key.
	 * @apiPermission Article Cant be Accessed permission name : api_article_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Articles.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of article.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ArticleNotFound Article data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_article_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'name_thai', 'name_eng', 'detail', 'detail_eng','img_article', 'video','link_youtube', 'created_by', 'created_at', 'updated_by', 'updated_at'];
		$data['article'] = $this->model_api_article->find($id, $select_field);

		if ($data['article']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Article',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Article not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /article/add Add Article.
	 * @apiVersion 0.1.0
	 * @apiName AddArticle
	 * @apiGroup article
	 * @apiHeader {String} X-Api-Key Articles unique access-key.
	 * @apiPermission Article Cant be Accessed permission name : api_article_add
	 *
 	 * @apiParam {String} Name_thai Mandatory name_thai of Articles. Input Name Thai Max Length : 50. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Articles. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Detail Mandatory detail of Articles.  
	 * @apiParam {String} Img_article Mandatory img_article of Articles. Input Img Article Max Length : 255. 
	 * @apiParam {String} Video Mandatory video of Articles. Input Video Max Length : 255. 
	 * @apiParam {String} Created_by Mandatory created_by of Articles. Input Created By Max Length : 50. 
	 * @apiParam {String} Created_at Mandatory created_at of Articles.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Articles. Input Updated By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Articles.  
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_article_add', false);

		$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
		$this->form_validation->set_rules('img_article', 'Img Article', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('video', 'Video', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'detail' => $this->input->post('detail'),
				'img_article' => $this->input->post('img_article'),
				'video' => $this->input->post('video'),
				'created_by' => $this->input->post('created_by'),
				'created_at' => $this->input->post('created_at'),
				'updated_by' => $this->input->post('updated_by'),
				'updated_at' => $this->input->post('updated_at'),
			];
			
			$save_article = $this->model_api_article->store($save_data);

			if ($save_article) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /article/update Update Article.
	 * @apiVersion 0.1.0
	 * @apiName UpdateArticle
	 * @apiGroup article
	 * @apiHeader {String} X-Api-Key Articles unique access-key.
	 * @apiPermission Article Cant be Accessed permission name : api_article_update
	 *
	 * @apiParam {String} Name_thai Mandatory name_thai of Articles. Input Name Thai Max Length : 50. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Articles. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Detail Mandatory detail of Articles.  
	 * @apiParam {String} Img_article Mandatory img_article of Articles. Input Img Article Max Length : 255. 
	 * @apiParam {String} Video Mandatory video of Articles. Input Video Max Length : 255. 
	 * @apiParam {String} Created_by Mandatory created_by of Articles. Input Created By Max Length : 50. 
	 * @apiParam {String} Created_at Mandatory created_at of Articles.  
	 * @apiParam {String} Updated_by Mandatory updated_by of Articles. Input Updated By Max Length : 50. 
	 * @apiParam {String} Updated_at Mandatory updated_at of Articles.  
	 * @apiParam {Integer} id Mandatory id of Article.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_status_post()
	{
		//$this->is_allowed('api_article_update', false);

	/* 	
		$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
		$this->form_validation->set_rules('img_article', 'Img Article', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('video', 'Video', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('updated_by', 'Updated By', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('updated_at', 'Updated At', 'trim|required'); */
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				/* 'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'detail' => $this->input->post('detail'),
				'img_article' => $this->input->post('img_article'),
				'video' => $this->input->post('video'),
				'created_by' => $this->input->post('created_by'),
				'created_at' => $this->input->post('created_at'),
				'updated_by' => $this->input->post('updated_by'),
				'updated_at' => $this->input->post('updated_at'), */
				'status' => $this->input->post('status'),
			];
			
			$save_article = $this->model_api_article->change($this->post('id'), $save_data);

			if ($save_article) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /article/delete Delete Article. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteArticle
	 * @apiGroup article
	 * @apiHeader {String} X-Api-Key Articles unique access-key.
	 	 * @apiPermission Article Cant be Accessed permission name : api_article_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Articles .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_article_delete', false);

		$article = $this->model_api_article->find($this->post('id'));

		if (!$article) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Article not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_article->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Article deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Article not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

}

/* End of file Article.php */
/* Location: ./application/controllers/api/Article.php */