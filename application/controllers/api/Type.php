<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Type extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_type');
	}

	/**
	 * @api {get} /type/all Get all types.
	 * @apiVersion 0.1.0
	 * @apiName AllType 
	 * @apiGroup type
	 * @apiHeader {String} X-Api-Key Types unique access-key.
	 * @apiPermission Type Cant be Accessed permission name : api_type_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Types.
	 * @apiParam {String} [Field="All Field"] Optional field of Types : id, name_thai, name_eng, image_type, created_at, created_by, status.
	 * @apiParam {String} [Start=0] Optional start index of Types.
	 * @apiParam {String} [Limit=10] Optional limit data of Types.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of type.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataType Type data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */

	/* public function save_type_post(){

		$save_data = [
			'name_thai' => $this->input->post('name_thai'),
			'name_eng' => $this->input->post('name_eng'),
			'image_type' => $this->input->post('image_type'),
			'created_by' => $this->input->post('created_by'),
			'status' => 'on',
		];

		
		$target_dir = base_url('/asset/img/');
		
		$target_file = $target_dir.basename($this->input->post('image_type'));
		if (move_uploaded_file($this->input->post('image_type'),$target_file)) {
		

			$message = 'Your data has been successfully stored into the database';

		} else {
			$message = cclang('data_not_change');
		}
		$save_type = $this->model_api_type->store($save_data);


	
		if ($save_type) {
			$this->response([
				'status' 	=> true,
				'message' 	=> $message,
				'target_dir' 	=> $_FILES["image_type"]["name"],
				//'target_file' 	=> $_FILES["image_type"]["error"]
			], API::HTTP_OK);
	
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> $message
			], API::HTTP_NOT_ACCEPTABLE);
		}

	} */
	
	public function all_get()
	{
		//$this->is_allowed('api_type_all', false);

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'name_thai', 'name_eng', 'image_type', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status'];
		$types = $this->model_api_type->get($filter, $field, null, $start, $select_field);
		$total = $this->model_api_type->count_all($filter, $field);

		$data['type'] = $types;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Type',
			'data'	 	=> $data,
			'total' 	=> $total
		], API::HTTP_OK);
	}

	
	/**
	 * @api {get} /type/detail Detail Type.
	 * @apiVersion 0.1.0
	 * @apiName DetailType
	 * @apiGroup type
	 * @apiHeader {String} X-Api-Key Types unique access-key.
	 * @apiPermission Type Cant be Accessed permission name : api_type_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Types.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of type.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError TypeNotFound Type data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_type_detail', false);

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'name_thai', 'name_eng', 'image_type', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status'];
		$data['type'] = $this->model_api_type->find($id, $select_field);

		if ($data['type']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Type',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Type not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /type/add Add Type.
	 * @apiVersion 0.1.0
	 * @apiName AddType
	 * @apiGroup type
	 * @apiHeader {String} X-Api-Key Types unique access-key.
	 * @apiPermission Type Cant be Accessed permission name : api_type_add
	 *
 	 * @apiParam {String} Name_thai Mandatory name_thai of Types. Input Name Thai Max Length : 50. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Types. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Image_type Mandatory image_type of Types. Input Image Type Max Length : 255. 
	 * @apiParam {String} Created_at Mandatory created_at of Types.  
	 * @apiParam {String} Created_by Mandatory created_by of Types. Input Created By Max Length : 30. 
	 * @apiParam {String} Status Mandatory status of Types. Input Status Max Length : 10. 
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_type_add', false);

		$this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('image_type', 'Image Type', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[30]');
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'image_type' => $this->input->post('image_type'),
				'created_at' => $this->input->post('created_at'),
				'created_by' => $this->input->post('created_by'),
				'status' => $this->input->post('status'),
			];
			
			$save_type = $this->model_api_type->store($save_data);

			if ($save_type) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /type/update Update Type.
	 * @apiVersion 0.1.0
	 * @apiName UpdateType
	 * @apiGroup type
	 * @apiHeader {String} X-Api-Key Types unique access-key.
	 * @apiPermission Type Cant be Accessed permission name : api_type_update
	 *
	 * @apiParam {String} Name_thai Mandatory name_thai of Types. Input Name Thai Max Length : 50. 
	 * @apiParam {String} Name_eng Mandatory name_eng of Types. Input Name Eng Max Length : 50. 
	 * @apiParam {String} Image_type Mandatory image_type of Types. Input Image Type Max Length : 255. 
	 * @apiParam {String} Created_at Mandatory created_at of Types.  
	 * @apiParam {String} Created_by Mandatory created_by of Types. Input Created By Max Length : 30. 
	 * @apiParam {String} Status Mandatory status of Types. Input Status Max Length : 10. 
	 * @apiParam {Integer} id Mandatory id of Type.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */


	

	public function update_status_post(){
		//$this->is_allowed('api_type_update', false);

		
		/* $this->form_validation->set_rules('name_thai', 'Name Thai', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('name_eng', 'Name Eng', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('image_type', 'Image Type', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('created_at', 'Created At', 'trim|required');
		$this->form_validation->set_rules('created_by', 'Created By', 'trim|required|max_length[30]'); */
		$this->form_validation->set_rules('status', 'Status', 'trim|required|max_length[10]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				/* 'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'image_type' => $this->input->post('image_type'),
				'created_at' => $this->input->post('created_at'),
				'created_by' => $this->input->post('created_by'), */
				'status' => $this->input->post('status'),
			];
			
			$save_type = $this->model_api_type->change($this->post('id'), $save_data);

			if ($save_type) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	public function update_type_post(){
		
			$save_data = [
				'name_thai' => $this->input->post('name_thai'),
				'name_eng' => $this->input->post('name_eng'),
				'updated_at' => date('Y-m-d H:i:s'),
				'updated_by' => $this->input->post('updated_by'),
			
			];
			
			$save_type = $this->model_api_type->change($this->post('id'), $save_data);

			if ($save_type) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		
	}
	
	/**
	 * @api {post} /type/delete Delete Type. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteType
	 * @apiGroup type
	 * @apiHeader {String} X-Api-Key Types unique access-key.
	 	 * @apiPermission Type Cant be Accessed permission name : api_type_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Types .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_type_delete', false);

		$type = $this->model_api_type->find($this->post('id'));

		if (!$type) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Type not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_type->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Type deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Type not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

}

/* End of file Type.php */
/* Location: ./application/controllers/api/Type.php */