<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Article Controller
*| --------------------------------------------------------------------------
*| Article site
*|
*/
class Article extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_article');
	}

	/**
	* show all Articles
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('article_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['articles'] = $this->model_article->get($filter, $field, $this->limit_page, $offset);
		$this->data['article_counts'] = $this->model_article->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/article/index/',
			'total_rows'   => $this->model_article->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Article List');
		$this->render('backend/standart/administrator/article/article_list', $this->data);
	}
	
	/**
	* Add new articles
	*
	*/
	public function add()
	{
		$this->is_allowed('article_add');

		$this->template->title('Article New');
		$this->render('backend/standart/administrator/article/article_add', $this->data);
	}

	/**
	* Add New Articles
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('article_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
		$this->form_validation->set_rules('article_img_article_name', 'Img Article', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('video', 'Video', 'trim|required|max_length[255]');
		

		if ($this->form_validation->run()) {
			$article_img_article_uuid = $this->input->post('article_img_article_uuid');
			$article_img_article_name = $this->input->post('article_img_article_name');
		
			$save_data = [
				'name' => $this->input->post('name'),
				'detail' => $this->input->post('detail'),
				'date_start' => date('Y-m-d H:i:s'),
				'video' => $this->input->post('video'),
			];

			if (!is_dir(FCPATH . '/uploads/article/')) {
				mkdir(FCPATH . '/uploads/article/');
			}

			if (!empty($article_img_article_name)) {
				$article_img_article_name_copy = date('YmdHis') . '-' . $article_img_article_name;

				rename(FCPATH . 'uploads/tmp/' . $article_img_article_uuid . '/' . $article_img_article_name, 
						FCPATH . 'uploads/article/' . $article_img_article_name_copy);

				if (!is_file(FCPATH . '/uploads/article/' . $article_img_article_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['img_article'] = $article_img_article_name_copy;
			}
		
			
			$save_article = $this->model_article->store($save_data);

			if ($save_article) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_article;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/article/edit/' . $save_article, 'Edit Article'),
						anchor('administrator/article', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/article/edit/' . $save_article, 'Edit Article')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/article');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/article');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Articles
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('article_update');

		$this->data['article'] = $this->model_article->find($id);

		$this->template->title('Article Update');
		$this->render('backend/standart/administrator/article/article_update', $this->data);
	}

	/**
	* Update Articles
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('article_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
		$this->form_validation->set_rules('article_img_article_name', 'Img Article', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('video', 'Video', 'trim|required|max_length[255]');
		
		if ($this->form_validation->run()) {
			$article_img_article_uuid = $this->input->post('article_img_article_uuid');
			$article_img_article_name = $this->input->post('article_img_article_name');
		
			$save_data = [
				'name' => $this->input->post('name'),
				'detail' => $this->input->post('detail'),
				'date_start' => date('Y-m-d H:i:s'),
				'video' => $this->input->post('video'),
			];

			if (!is_dir(FCPATH . '/uploads/article/')) {
				mkdir(FCPATH . '/uploads/article/');
			}

			if (!empty($article_img_article_uuid)) {
				$article_img_article_name_copy = date('YmdHis') . '-' . $article_img_article_name;

				rename(FCPATH . 'uploads/tmp/' . $article_img_article_uuid . '/' . $article_img_article_name, 
						FCPATH . 'uploads/article/' . $article_img_article_name_copy);

				if (!is_file(FCPATH . '/uploads/article/' . $article_img_article_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['img_article'] = $article_img_article_name_copy;
			}
		
			
			$save_article = $this->model_article->change($id, $save_data);

			if ($save_article) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/article', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/article');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/article');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Articles
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('article_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'article'), 'success');
        } else {
            set_message(cclang('error_delete', 'article'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Articles
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('article_view');

		$this->data['article'] = $this->model_article->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Article Detail');
		$this->render('backend/standart/administrator/article/article_view', $this->data);
	}
	
	/**
	* delete Articles
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$article = $this->model_article->find($id);

		if (!empty($article->img_article)) {
			$path = FCPATH . '/uploads/article/' . $article->img_article;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_article->remove($id);
	}
	
	/**
	* Upload Image Article	* 
	* @return JSON
	*/
	public function upload_img_article_file()
	{
		if (!$this->is_allowed('article_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'article',
		]);
	}

	/**
	* Delete Image Article	* 
	* @return JSON
	*/
	public function delete_img_article_file($uuid)
	{
		if (!$this->is_allowed('article_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'img_article', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'article',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/article/'
        ]);
	}

	/**
	* Get Image Article	* 
	* @return JSON
	*/
	public function get_img_article_file($id)
	{
		if (!$this->is_allowed('article_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$article = $this->model_article->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'img_article', 
            'table_name'        => 'article',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/article/',
            'delete_endpoint'   => 'administrator/article/delete_img_article_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('article_export');

		$this->model_article->export('article', 'article');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('article_export');

		$this->model_article->pdf('article', 'article');
	}
}


/* End of file article.php */
/* Location: ./application/controllers/administrator/Article.php */