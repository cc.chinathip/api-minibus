<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Activity Controller
*| --------------------------------------------------------------------------
*| Activity site
*|
*/
class Activity extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_activity');
	}

	/**
	* show all Activitys
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('activity_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['activitys'] = $this->model_activity->get($filter, $field, $this->limit_page, $offset);
		$this->data['activity_counts'] = $this->model_activity->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/activity/index/',
			'total_rows'   => $this->model_activity->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Activity List');
		$this->render('backend/standart/administrator/activity/activity_list', $this->data);
	}
	
	/**
	* Add new activitys
	*
	*/
	public function add()
	{
		$this->is_allowed('activity_add');

		$this->template->title('Activity New');
		$this->render('backend/standart/administrator/activity/activity_add', $this->data);
	}

	/**
	* Add New Activitys
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('activity_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
		$this->form_validation->set_rules('activity_img_activity_name', 'Img Activity', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('date_end', 'Date End', 'trim|required');
		

		if ($this->form_validation->run()) {
			$activity_img_activity_uuid = $this->input->post('activity_img_activity_uuid');
			$activity_img_activity_name = $this->input->post('activity_img_activity_name');
		
			$save_data = [
				'name' => $this->input->post('name'),
				'detail' => $this->input->post('detail'),
				'date_start' => date('Y-m-d H:i:s'),
				'date_end' => $this->input->post('date_end'),
			];

			if (!is_dir(FCPATH . '/uploads/activity/')) {
				mkdir(FCPATH . '/uploads/activity/');
			}

			if (!empty($activity_img_activity_name)) {
				$activity_img_activity_name_copy = date('YmdHis') . '-' . $activity_img_activity_name;

				rename(FCPATH . 'uploads/tmp/' . $activity_img_activity_uuid . '/' . $activity_img_activity_name, 
						FCPATH . 'uploads/activity/' . $activity_img_activity_name_copy);

				if (!is_file(FCPATH . '/uploads/activity/' . $activity_img_activity_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['img_activity'] = $activity_img_activity_name_copy;
			}
		
			
			$save_activity = $this->model_activity->store($save_data);

			if ($save_activity) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_activity;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/activity/edit/' . $save_activity, 'Edit Activity'),
						anchor('administrator/activity', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/activity/edit/' . $save_activity, 'Edit Activity')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/activity');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/activity');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Activitys
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('activity_update');

		$this->data['activity'] = $this->model_activity->find($id);

		$this->template->title('Activity Update');
		$this->render('backend/standart/administrator/activity/activity_update', $this->data);
	}

	/**
	* Update Activitys
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('activity_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
		$this->form_validation->set_rules('activity_img_activity_name', 'Img Activity', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('date_end', 'Date End', 'trim|required');
		
		if ($this->form_validation->run()) {
			$activity_img_activity_uuid = $this->input->post('activity_img_activity_uuid');
			$activity_img_activity_name = $this->input->post('activity_img_activity_name');
		
			$save_data = [
				'name' => $this->input->post('name'),
				'detail' => $this->input->post('detail'),
				'date_start' => date('Y-m-d H:i:s'),
				'date_end' => $this->input->post('date_end'),
			];

			if (!is_dir(FCPATH . '/uploads/activity/')) {
				mkdir(FCPATH . '/uploads/activity/');
			}

			if (!empty($activity_img_activity_uuid)) {
				$activity_img_activity_name_copy = date('YmdHis') . '-' . $activity_img_activity_name;

				rename(FCPATH . 'uploads/tmp/' . $activity_img_activity_uuid . '/' . $activity_img_activity_name, 
						FCPATH . 'uploads/activity/' . $activity_img_activity_name_copy);

				if (!is_file(FCPATH . '/uploads/activity/' . $activity_img_activity_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['img_activity'] = $activity_img_activity_name_copy;
			}
		
			
			$save_activity = $this->model_activity->change($id, $save_data);

			if ($save_activity) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/activity', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/activity');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/activity');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Activitys
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('activity_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'activity'), 'success');
        } else {
            set_message(cclang('error_delete', 'activity'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Activitys
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('activity_view');

		$this->data['activity'] = $this->model_activity->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Activity Detail');
		$this->render('backend/standart/administrator/activity/activity_view', $this->data);
	}
	
	/**
	* delete Activitys
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$activity = $this->model_activity->find($id);

		if (!empty($activity->img_activity)) {
			$path = FCPATH . '/uploads/activity/' . $activity->img_activity;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_activity->remove($id);
	}
	
	/**
	* Upload Image Activity	* 
	* @return JSON
	*/
	public function upload_img_activity_file()
	{
		if (!$this->is_allowed('activity_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'activity',
		]);
	}

	/**
	* Delete Image Activity	* 
	* @return JSON
	*/
	public function delete_img_activity_file($uuid)
	{
		if (!$this->is_allowed('activity_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'img_activity', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'activity',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/activity/'
        ]);
	}

	/**
	* Get Image Activity	* 
	* @return JSON
	*/
	public function get_img_activity_file($id)
	{
		if (!$this->is_allowed('activity_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$activity = $this->model_activity->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'img_activity', 
            'table_name'        => 'activity',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/activity/',
            'delete_endpoint'   => 'administrator/activity/delete_img_activity_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('activity_export');

		$this->model_activity->export('activity', 'activity');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('activity_export');

		$this->model_activity->pdf('activity', 'activity');
	}
}


/* End of file activity.php */
/* Location: ./application/controllers/administrator/Activity.php */