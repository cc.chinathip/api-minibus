<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_users extends MY_Model {

	private $primary_key 	= 'id';
	private $table_name 	= 'users';
	private $field_search 	= ['name', 'surname', 'username', 'password', 'email', 'address', 'tel', 'facebook_id', 'line_id', 'type_user'];

	public function __construct()
	{
		$config = array(
			'primary_key' 	=> $this->primary_key,
		 	'table_name' 	=> $this->table_name,
		 	'field_search' 	=> $this->field_search,
		 );

		parent::__construct($config);
	}

	public function userbyId_data($user_id){

		$data = $this->db->query("SELECT * FROM users WHERE id = '" . $user_id . "'  ");
		
		return $data->result();


	}



	public function user_all_data($type_user,$total=null){

		$data = $this->db->query("SELECT * FROM users WHERE type_user = '" . $type_user . "'  ");
		
		if(isset($total) AND $total=='total'){

			return $data->num_rows();

		}else{
			return $data->result();
		}

		

	}

	public function username_password_data($username,$password,$type_user,$total=null){

		$data = $this->db->query("SELECT * FROM users WHERE username = '" . $username . "' AND password = '" . $password . "' AND  type_user = '" . $type_user . "'   ");
		
		if(isset($total) AND $total=='total'){

			return $data->num_rows();

		}else{
			return $data->result();
		}

		

	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
		$field = $this->scurity($field);

        if (empty($field)) {
	        foreach ($this->field_search as $field) {
	            if ($iterasi == 1) {
	                $where .= "users.".$field . " LIKE '%" . $q . "%' ";
	            } else {
	                $where .= "OR " . "users.".$field . " LIKE '%" . $q . "%' ";
	            }
	            $iterasi++;
	        }

	        $where = '('.$where.')';
        } else {
        	$where .= "(" . "users.".$field . " LIKE '%" . $q . "%' )";
        }

		$this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
	{
		$iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
		$field = $this->scurity($field);

        if (empty($field)) {
	        foreach ($this->field_search as $field) {
	            if ($iterasi == 1) {
	                $where .= "users.".$field . " LIKE '%" . $q . "%' ";
	            } else {
	                $where .= "OR " . "users.".$field . " LIKE '%" . $q . "%' ";
	            }
	            $iterasi++;
	        }

	        $where = '('.$where.')';
        } else {
        	$where .= "(" . "users.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
        	$this->db->select($select_field);
        }
		
		$this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
        $this->db->order_by('users.'.$this->primary_key, "DESC");
		$query = $this->db->get($this->table_name);

		return $query->result();
	}

    public function join_avaiable() {
        
        return $this;
    }

    public function filter_avaiable() {
        
        return $this;
    }

}

/* End of file Model_users.php */
/* Location: ./application/models/Model_users.php */