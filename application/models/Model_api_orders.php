<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_api_orders extends MY_Model {

	private $primary_key 	= 'id';
	private $table_name 	= 'orders';
	private $field_search 	= ['id', 'user_id', 'name', 'email', 'tel', 'province_id', 'amphure_id', 'dealer_id', 'qty_buy', 'date_buy', 'updated_at', 'updated_by', 'type_orders'];

	public function __construct()
	{
		$config = array(
			'primary_key' 	=> $this->primary_key,
		 	'table_name' 	=> $this->table_name,
		 	'field_search' 	=> $this->field_search,
		 );

		parent::__construct($config);
	}

	public function allorder_data($type_order){

		$query = $this->db->query("SELECT orders.*,province.PROVINCE_NAME,amphur.AMPHUR_NAME,dealer.name_thai as name_dealer FROM orders 
		LEFT JOIN province ON orders.province_id = province.PROVINCE_ID
		LEFT JOIN amphur ON orders.amphure_id = amphur.AMPHUR_ID
		LEFT JOIN dealer ON orders.dealer_id = dealer.id
		WHERE orders.type_orders = '".$type_order."'
		");
		return $query->result();

	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
		$field = $this->scurity($field);

        if (empty($field)) {
	        foreach ($this->field_search as $field) {
	            if ($iterasi == 1) {
	                $where .= $field . " LIKE '%" . $q . "%' ";
	            } else {
	                $where .= "OR " . $field . " LIKE '%" . $q . "%' ";
	            }
	            $iterasi++;
	        }

	        $where = '('.$where.')';
        } else {
        	$where .= "(" . $field . " LIKE '%" . $q . "%' )";
        }

        $this->db->where($where);
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
	{
		$iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
		$field = $this->scurity($field);

        if (empty($field)) {
	        foreach ($this->field_search as $field) {
	            if ($iterasi == 1) {
	                $where .= $field . " LIKE '%" . $q . "%' ";
	            } else {
	                $where .= "OR " . $field . " LIKE '%" . $q . "%' ";
	            }
	            $iterasi++;
	        }

	        $where = '('.$where.')';
        } else {
        	if (in_array($field, $select_field)) {
        		$where .= "(" . $field . " LIKE '%" . $q . "%' )";
        	}
        }

        if (is_array($select_field) AND count($select_field)) {
        	$this->db->select($select_field);
        }
		
		if ($where) {
        	$this->db->where($where);
		}
        $this->db->limit($limit, $offset);
        $this->db->order_by($this->primary_key, "DESC");
		$query = $this->db->get($this->table_name);

		return $query->result();
	}

}

/* End of file Model_orders.php */
/* Location: ./application/models/Model_orders.php */